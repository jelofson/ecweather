<?php
/**
 * Class for displaying a weather widget based on Environment Canada Weather
 * 
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * 
 * @license http://www.gnu.org/licenses/ GNU General Purpose License v3
 * 
 * Copyright (C) 2014  Jon Elofson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 * 
 */
namespace Ecweather;

class Weather 
{
    /**
     * The URL to the XML from Environment Canada
     * 
     * http://dd.weatheroffice.ec.gc.ca/citypage_weather/
     * 
     * @var string 
     */
    protected $url;
    
    /**
     * The SimpleXMLElement object after importing
     * 
     * @var SimpleXMLElement
     */
    protected $simplexml;
    
    /**
     * Path to a directory for optionally caching the xml
     * 
     * @var string 
     */
    protected $cachePath;
    
    /**
     * The number of seconds before the cached version expires
     * 
     * @var int 
     */
    protected $cacheExpire;

    /**
     * Number of tabs to indent when building the html output
     * 
     * @var int 
     */
    protected static $indent = 0;

    /**
     * The link to the Environment Canada forecast (not currently in the xml)
     * 
     * @var string
     */
    protected $forecastLink;
    
    /**
     * An array that stores currrent conditions. 
     * 
     * Keys are:
     * 
     * `icon` - The image of the current condition
     * 
     * `condition` - Current condition (Mainly sunny, etc.)
     * 
     * `temperature` - Temp in celcius
     * 
     * `time` - when issued (H:i T)
     * 
     * `wind` - wind speed and direction (with gust if present)
     * 
     * @see extractCurrentConditions()
     * 
     * @var array 
     */
    protected $currentConditions = [];
    
    /**
     * An array that stores the short-term forecast
     * 
     * Keys are:
     * 
     * `period` - When (Today, Tonight, etc)
     * 
     * `icon` - The image for the forecasted condition
     * 
     * `summary` - Brief summary of the forecast
     * 
     * @see extractForecastShort()
     * 
     * @var array 
     */
    protected $forecastShort = [];
    
    /**
     * An array of long-term forecast data
     * 
     * Keys are:
     * 
     * `period` - When (Today, Tonight, etc)
     * 
     * `icon` - The image for the forecasted condition
     * 
     * `summary` - Brief summary of the forecast
     * 
     * @see extractForecastLong()
     * 
     * @var array 
     */
    protected $forecastLong = [];
    
    /**
     * An array of warnings, if any
     * 
     * Keys are:
     * 
     * `url` - The url to the warning web page
     * 
     * `type` - warning, watch, ended, statement
     * 
     * `priority` - low, medium, high
     * 
     * `description` - Brief description (Snowfall warning, etc)
     * 
     * `issued` - When issued
     * 
     * @see extractWarnings()
     * 
     * @var array
     */
    protected $warnings = [];
    
    /**
     * The location name with province (Edmonton, AB)
     * 
     * @var string 
     */
    protected $locationName;
    
    /**
     * The station name (Edmonton City Centre Airport)
     * 
     * @var string 
     */
    protected $stationName;
    
    /**
     * The station code (yxd)
     * 
     * @var string
     */
    protected $stationCode;

    /**
     * The name code (s0000045)
     * 
     * @var string
     */
    protected $nameCode;

    
    /**
     * The base url to the weather icons
     * 
     * @var string
     */
    protected $iconBase = '//weather.gc.ca/weathericons';
    
    /**
     * The URL to Environment Canada's FIP signature image
     * 
     * @var string
     */
    protected $fipUrl = '//weather.gc.ca/images/ecfip_e.gif';

    /**
     * The Base URL to Environment Canada's forecasts
     * 
     * @var string
     */
    protected $forecastBase = 'http://weather.gc.ca/city/pages';

    /**
     * Strings that can be translated. You can pass an array with the same keys
     * to the constructor to override these.
     * 
     * 
     * @var array
     */
    protected $strings = [
        'TEXT_FIP'=>'Environment Canada',
        'TEXT_WIND'=>'Wind: ',
        'TEXT_TOGGLE'=>'Toggle Short-term Forecast',
        'TEXT_NOT_OBSERVED'=>'Not Observed',
        'TEXT_GUST'=>'gust',
        'FORMAT_LOCATION'=>'%s, %s'
    ];

    /**
     * Map of weather station codes and name codes
     *
     * @var array
     */
    protected $wx_codes = [];

    /**
     * Constructor 
     * 
     * <code>
     * $url = 'http://dd.weatheroffice.ec.gc.ca/citypage_weather/xml/AB/s0000045_e.xml';
     * $cachePath = '/path/to/cache';
     * $cacheExpire = 1800;
     *
     * // Optionally override some strings
     * $strings = ['TEXT_WIND'=>'Vent : '];
     *
     * $weather = new \Ecweather\Weather($url, $cachePath, $cacheExpire, $strings);
     * 
     * 
     * echo $weather->display();
     * 
     * </code>
     * 
     * @param string $url The url to the xml file from environment canada
     * @param string $cachePath Path to optional cache folder
     * @param int $cacheExpire Seconds the cached version is valid (default 900)
     * @param array $strings Text that can be translated (or localized)
     * @throws \Exception
     */
    public function __construct($url, $cachePath = null, $cacheExpire = 900, $strings = null)
    {
        $this->url = $url;
        $this->cachePath = $cachePath;
        $this->cacheExpire = (int) $cacheExpire;

        if ($strings) {
            $this->strings = array_merge($this->strings, (array) $strings);
        }

        $this->simplexml = $this->loadXml();
        
        if ($this->simplexml === false) {
            throw new \Exception('Error with XML');
        }

        $this->loadWxCodes();
        $this->parseData();
        
    }

    /**
     * Parse the xml data into parts we want. 
     * 
     * @return void
     */
    protected function parseData()
    {
        $city = (string) $this->simplexml->location->name;
        $prov = strtoupper($this->simplexml->location->province->attributes()->code);
        $this->locationName =  sprintf($this->strings['FORMAT_LOCATION'], $city, $prov); 
        
        if (! property_exists($this->simplexml->currentConditions, 'station')) {
            $this->currentConditions = false;
            return false;
        }
        
        $this->stationName = (string) $this->simplexml->currentConditions->station;
        $this->stationCode = (string) $this->simplexml->currentConditions->station->attributes()->code;

        $this->nameCode = (string) $this->simplexml->location->name->attributes()->code;
        
        $this->currentConditions = $this->extractCurrentConditions();
        $this->forecastShort = $this->extractForecastShort();
        $this->forecastLong = $this->extractForecastLong();
        $this->warnings = $this->extractWarnings();

        $this->forecastLink = $this->buildForecastLink();

    }

    protected function loadWxCodes()
    {
        $file = file_get_contents(__DIR__ . '/wx_codes.data');

        if ($file !== false) {
            $this->wx_codes = unserialize($file);
        }
    }

    protected function buildForecastLink()
    {
        if (count($this->wx_codes) > 0) {

            $forecast_code = $this->wx_codes[$this->nameCode];

            if ($forecast_code !== false) {
                $_pos = strrpos($this->url, '_') ;
                $lang = substr($this->url, $_pos, 2);
                $url = $this->forecastBase . '/' . $forecast_code . '_metric' . $lang . '.html';

                return $url;
            }
            return false;
        }
        return false;
    }

    

    /**
     * Build the main container part of the html output
     * 
     * @return string Html
     */
    protected function buildOuterContainer()
    {
        $html = [];
        $html[] = $this->indent() . '<div class="ec-weather-widget">';
        $html[] = '--warnings--';
        $html[] = '--current--';
        $html[] = '--forecast--';
        $html[] = $this->indent() . '</div>';
        return implode(PHP_EOL, $html);
    }

    /**
     * Build the warnings part of the html output
     * 
     * @return string Html
     */
    protected function buildWarnings()
    {
        $html = [];
        if (count($this->warnings) > 0) {
            $this->indent(1);
            foreach ($this->warnings as $warning) {
                $html[] = sprintf($this->indent() . '<div class="ec-warning %s"><a href="%s" title="%s">!</a></div>', $warning['type'], $warning['url'], $warning['description']);
            }
            $this->indent(-1);
        }

        return implode(PHP_EOL, $html);
    }

    /**
     * Build the current conditions part of the html output
     * 
     * @return string Html
     */
    protected function buildCurrent()
    {
        $html = [];
        $html[] = $this->indent(1) . sprintf('<h1><a href="%s">%s</a></h1>', $this->forecastLink, $this->locationName);
        $html[] = $this->indent() . '<div class="ec-current-weather">';
        $html[] = $this->indent(1) . '<div class="ec-weather-icon">';
        if ($this->currentConditions['icon']) {
            $currentIconSrc = $this->iconBase . '/' . $this->currentConditions['icon'];
            $html[] = $this->indent(1) . sprintf('<img src="%s" alt="%s" title="%s" />', $currentIconSrc, $this->currentConditions['condition'], $this->currentConditions['condition']); 
        } else {
            $html[] = $this->indent(1) . '&nbsp;';
        }

        $html[] = $this->indent(-1) . '</div>';
        $html[] = $this->indent() . '<div class="ec-temp-wind">';
        $html[] = $this->indent(1) . '<div class="ec-temp">' . $this->currentConditions['temperature'] .  ' &deg;C</div>';
        $html[] = $this->indent() . '<div class="ec-wind">' . $this->strings['TEXT_WIND'] . $this->currentConditions['wind'] . '</div>';
        $html[] = $this->indent(-1) . '</div>';
        $html[] = $this->indent(-1) . '</div>';
        return implode(PHP_EOL, $html);
    }

    /**
     * Build the forecast part of the html output
     * 
     * @return string Html
     */
    protected function buildForecast()
    {
        $html = [];
        $html[] = $this->indent() . '<div class="ec-forecast" data-toggle-text="' . $this->strings['TEXT_TOGGLE'] . '">';
        $this->indent(1);
        foreach ($this->forecastShort as $forecast) {
            $html[] = $this->indent() . '<div class="ec-forecast-container">';
            $html[] = $this->indent(1) . '<div>' . $forecast['period'] . '</div>';

            $forecastIconSrc = $this->iconBase . '/' . $forecast['icon'];
            
            $html[] = $this->indent() . '<div class="ec-forecast-icon">';
            $html[] = $this->indent(1) . sprintf('<img src="%s" alt="%s" title="%s" />', $forecastIconSrc, $forecast['summary'], $forecast['summary']);
            $html[] = $this->indent(-1) . '</div>';
            $html[] = $this->indent() . '<div class="ec-forecast-summary">' . $forecast['summary'] . '</div>';
            $html[] = $this->indent(-1) . '</div>';
        }

        $html[] = $this->indent(-1) . '</div>';
        $html[] = $this->indent() . '<div>';
        $html[] = $this->indent(1) . sprintf('<a href="%s" title="%s">', $this->forecastLink, $this->strings['TEXT_FIP']) . sprintf('<img src="%s" alt="%s"/>', $this->fipUrl, $this->strings['TEXT_FIP']) . '</a>';
        $html[] = $this->indent(-1) . '</div>';

        return implode(PHP_EOL, $html);
    }

    /**
     * Indent a sting by 4 spaces multiplied by $count. This is based on a static
     * indent amount.
     * 
     * @param int $count
     * @return string Spaces for indenting
     */
    protected function indent($count=null)
    {
        if ($count) {
            self::$indent += $count;
        }
        if (self::$indent < 0) {
            self::$indent = 0;
        }
        return str_repeat(" ", self::$indent * 4);
    }
    
    /**
     * Load xml from cache or from the URL
     * 
     * @return SimpleXMLElement
     */
    protected function loadXml()
    {
        if ($this->cachePath && file_exists($this->cachePath)) {
            $xml = $this->loadFromCache();
        } else {
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $data = curl_exec($curl);
            curl_close($curl);
            $xml = simplexml_load_string($data);
        }
        
        return $xml;
    }
    
    /**
     * Load xml from cache (or load from URL and re-cache if expired)
     * 
     * @return SimpleXMLElement
     */
    protected function loadFromCache()
    {
        $this->cachePath = rtrim($this->cachePath, '/');
        $filename = substr($this->url, strrpos($this->url, '/') + 1);
        $expire = time() - $this->cacheExpire;
        $filename = $this->cachePath . '/' . $filename;

        if (file_exists($filename) && filemtime($filename) >= $expire) {
            $xml = simplexml_load_file($filename);

            if ($xml === false && is_writable($filename)) {
                unlink($filename);
                $xml = simplexml_load_file($this->url);
            }
        } else {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $data = curl_exec($curl);
            curl_close($curl);
            $xml = simplexml_load_string($data);

            if ($xml !== false && is_writable($this->cachePath)) {
                file_put_contents($filename, $xml->asXml());
            }
        }

        return $xml;
    }
    
        
    /**
     * Extracts the current conditions data from the xml and returns an array
     * 
     * @see $currentConditions
     * @return array
     */
    protected function extractCurrentConditions()
    {
        $timestamp = strtotime($this->simplexml->currentConditions->dateTime[0]->timeStamp . ' UTC');
        $wind = $this->extractWind();

        $iconCode = (string) $this->simplexml->currentConditions->iconCode;
        if (empty($iconCode)) {
            $icon = false;
        } else {
            $icon = $iconCode . '.gif';
        }
        $condition = (string) $this->simplexml->currentConditions->condition;
        if (empty($condition)) {
            $condition = $this->strings['TEXT_NOT_OBSERVED'];
        }

        $currentConditions = [
            'icon'=>$icon,
            'condition'=> $condition,
            'temperature'=> (string) $this->simplexml->currentConditions->temperature,
            'time'=>date('H:i T', $timestamp),
            'wind'=>$wind
        ];

        return $currentConditions;
    }
    
    /**
     * Extracts wind data from the xml and returns as a string to the 
     * currentConditions array
     * 
     * @see $currentConditions
     * @return string
     */
    protected function extractWind()
    {
        $wind = $this->simplexml->currentConditions->wind;
        $speed = (string) $wind->speed;
        $direction = (string) $wind->direction;
        $units = (string) $wind->speed->attributes()->units;
        
        if (empty($speed)) {
            return $this->strings['TEXT_NOT_OBSERVED'];
        }

        $gust = (string) $wind->gust;
        if ($gust) {
            return $direction . ' ' . $speed . ' ' . $units . '<br/>' . "({$this->strings['TEXT_GUST']} $gust)";
        } else {
            return $direction . ' ' . $speed . ' ' . $units;
        }
        
    }

    /**
     * Extracts the short-term forecast data from the xml and returns an array
     * 
     * @see $forecastShort
     * @return array
     */
    protected function extractForecastShort()
    {
        $forecastGroup = $this->simplexml->forecastGroup;
        $count = 0;
        $forecastShort = [];
        foreach ($forecastGroup->forecast as $forecast) {
            if ($count < 3) {
                $forecastShort[] = [
                    'period'=> (string) $forecast->period->attributes()->textForecastName,
                    'icon'=> (string) $forecast->abbreviatedForecast->iconCode . '.gif',
                    'summary'=> (string) $forecast->temperatures->textSummary
                ];
                
            }
            $count++;
        }
        return $forecastShort;
    }
    
    /**
     * Extracts the long-term forecast data from the xml and returns an array
     * 
     * @see $forecastLong
     * @return array
     */
    protected function extractForecastLong()
    {
        $forecastGroup = $this->simplexml->forecastGroup;
        
        $forecastLong = [];
        foreach ($forecastGroup->forecast as $forecast) {
            $forecastLong[] = [
                'period'=> (string) $forecast->period,
                'icon'=> (string) $forecast->abbreviatedForecast->iconCode . '.gif',
                'summary'=> (string) $forecast->textSummary
            ];
                
        }
        return $forecastLong;
    }
    
    /**
     * Extracts warnings data (if present) from the xml and returns an array
     * 
     * @see $warnings
     * @return array
     */
    protected function extractWarnings()
    {

        $warnings = [];
        $numEvents = count($this->simplexml->warnings[0]->event);

        if ($numEvents > 0) {
            
            foreach ($this->simplexml->warnings[0]->event as $event) {
                $issuedTS = strtotime($event->dateTime[0]->timeStamp . ' UTC');
                $warnings[] = [
                    'url'=> (string) $this->simplexml->warnings->attributes()->url,
                    'type'=> (string) $event->attributes()->type,
                    'priority'=> (string) $event->attributes()->priority,
                    'description'=> (string) $event->attributes()->description,
                    'issued'=>date('H:i T', $issuedTS)
                ];
            }
        }

        return $warnings;
    }

    /**
     * Get the array of current conditions
     *
     * @return array Current Conditions
     */
    public function getCurrentConditions()
    {
        return $this->currentConditions;
    }

    /**
     * Get the short-term forecast
     *
     * @return array Short-term forecast data
     */
    public function getForecastShort()
    {
        return $this->forecastShort;
    }

    /**
     * Get the long-term forecast
     *
     * @return array Long-term forecast data
     */
    public function getForecastLong()
    {
        return $this->forecastLong;
    }

    /**
     * Get the warnings, if any
     *
     * @return array weather warnings
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * Get the station code
     *
     * @return string station code
     */
    public function getStationCode()
    {
        return $this->stationCode;
    }

    /**
     * Get the location name
     *
     * @return string location name
     */
    public function getLocationName()
    {
        return $this->locationName;
    }


    /**
     * Get fipurl
     * 
     * @return string
     */
    public function getFipUrl()
    {
        return $this->fipUrl;
    }

    /**
     * Get iconBase
     * 
     * @return string
     */
    public function getIconBase()
    {
        return $this->iconBase;
    }

    /**
     * Set static indent level. Fluent
     * 
     * @param int $indent
     * @return Weather The current object
     */
    public function setIndent($indent)
    {
        self::$indent = $indent;
        return $this;
    }

    /**
     * set fipurl
     * 
     * @param string Url to fip image
     * @return void
     */
    public function setFipUrl($url)
    {
        $this->fipUrl = $url;
    }

    /**
     * set forecastBase. 
     * 
     * @param string Url to forecast Base url
     * @return void
     */
    public function setForecastBase($url)
    {
        $this->forecastBase = $url;
        // Rebuild the forecastLink with new base
        $this->forecastLink = $this->buildForecastLink();

    }
    
    /**
     * set the link to the weather.gc.ca forecast web page
     * 
     * @param string Url to forecast url
     * @return void
     */
    public function setForecastLink($url)
    {
        $this->forecastLink = $url;
    }

    /**
     * set iconBase
     * 
     * @param string Url to icon base
     * @return void
     */
    public function setIconBase($url)
    {
        $this->iconBase = rtrim($url, '/');
    }

    /**
     * Display the output in HTML
     * 
     * @return string HTML for the weather data
     */
    public function display()
    {
        
        $outer = $this->buildOuterContainer();
        $warnings = $this->buildWarnings();
        $current = $this->buildCurrent();
        $forecast = $this->buildForecast();

        $output = str_replace('--warnings--', $warnings, $outer);
        $output = str_replace('--current--', $current, $output);
        $output = str_replace('--forecast--', $forecast, $output);

        return $output;

    }
}
