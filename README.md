# README #

This class is for creating an HTML weather widget on any web page. It relies on Environment Canada's weather data available at http://data.gc.ca/data/en/dataset/1f864766-7f7f-4be7-8292-295065c65c78. This data is part of the Government of Canada Open Data initiative http://open.canada.ca/en/.

Find your weather data at: http://dd.weatheroffice.ec.gc.ca/citypage_weather/xml/


### Requirements ###

* PHP 5.4 (due to short array syntax)
* jQuery (if using javascript to toggle the short term forecast)

### How do I get set up? ###

* Download or clone the repository
* Copy the css/ec-weather.css and js/ec-weather.js files into your html folder, just like any other assets.
* Copy the Ecweather folder into your code library. The class should be [PSR-4 autoloader](http://www.php-fig.org/psr/psr-4/) compliant.
* Include the Ecweather.php file in your PHP code if not using an autoloader.
* Add the css and js files to your html being sure the JS file comes after your jquery include.


```
#!php

<?php 
// This should go in some sort of controller, or include, but you get the idea.
$xml_url = 'http://dd.weatheroffice.ec.gc.ca/citypage_weather/xml/AB/s0000045_e.xml';

$cache_path = '/my/cache'; // must be writable
$cache_expire = 1800; // default is 900

// cache_path and cache_expire are optional. If you don't include a cache_path, it will load 
// the url from ec.gc.ca on each page request. This is not recommended
$weather = new \Ecweather\Weather($xml_url, $cache_path, $cache_expire);

// You can also override default strings if you want some translation done
$strings = [
    'TEXT_WIND'=>'Vent : ',
    'TEXT_FIP'=>'Environnement Canada',
    'TEXT_TOGGLE'=>'Afficher ou masquer des prévisions à court terme', // used if including the js file
    'TEXT_NOT_OBSERVED'=>'Pas observé',
    'TEXT_GUST'=>'coup de vent',
    'FORMAT_LOCATION'=>'%s (%s)' // city and province, respectively
];

// Pass this array as the last argument in the constructor
$weather = new \Ecweather\Weather($xml_url, $cache_path, $cache_expire, $strings);

// You can also override the forecastLink 
$weather->setForecastLink('http://your.custom.url/forecast.html');

// OR just override the base url to forecasts, which should not be necessary, but possible.
// The forecastBase is the base url for links to the weather.gc.ca page
// For example: http://weather.gc.ca/city/pages is the default
$weather->setForecastBase('http://weather.gc.ca/city/current');

?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Your css here -->
        <link rel="stylesheet" type="text/css" media="screen" href="/path/to/css/ec-weather.css" />
        
    </head>
    <body>
<?php 
echo $weather->display(); 


// OR preset the initial indent, if you have OCD
echo $weather->setIndent(2)->display(); 

?>
        <script src="/path/to/jquery/jquery-1.8.2.js"></script>
        <script src="/path/to/js/ec-weather.js"></script>
    </body>
</html>
```

### Notes ###

The cache and cache expiry are optional, but recommended. If you don't include them, it won't use a cache. 

The default cache expiry is 900 seconds.

**You don't have to include the JavaScript file**. All it does is create a short-term forecast toggle that maintains state via localStorage.


### What it looks like ###

This is an image

![ecweather.png](https://bitbucket.org/repo/7GodjL/images/3007779431-ecweather.png)