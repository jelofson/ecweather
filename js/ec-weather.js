$(function() {
    $("div.ec-forecast").each(function(index) {
        var toggle_text = ($(this).attr('data-toggle-text'));
        var toggle_div = $('<div class="ec-toggle-forecast"></div>'); 
        var toggle_link = $('<a href="#" title="' + toggle_text + '">&#9650;</a>');
        toggle_div.html(toggle_link);
        
        $(this).after(toggle_div);
        var localStorageID = 'ec-forecast-' + index;

        $(toggle_link).on('click', function(e) {
            var forecast_div = $(this).parents('div.ec-toggle-forecast').prev();

            if ($(forecast_div).is(':visible')) {
                $(this).html("&#9660;");
                $(forecast_div).hide();
                if (supports_html5_storage()) {
                    localStorage.setItem(localStorageID, 'hide');
                }
                
            } else {
                $(this).html('&#9650;');
                $(forecast_div).show();
                if (supports_html5_storage()) {
                    localStorage.setItem(localStorageID, 'show');
                }
                
            }
            
            e.preventDefault();
        });
        if (supports_html5_storage()) {
            var ec_weather = localStorage.getItem(localStorageID);
            if (ec_weather === 'hide') {
                $(this).hide();
                $(toggle_link).html('&#9660;');
            }
        }

    });
        
});

function supports_html5_storage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}


