<?php
namespace Ecweather\Tests;

include '../Weather.php';
use \Ecweather\Weather as Weather;

class WeatherTest extends \PHPUnit_Framework_TestCase
{
    protected $weatherNoCache;
    protected $weatherWithCache;
    protected $weatherNoWind;
    protected $weatherStrings;

    protected function setUp()
    {
        $url = './s0000727_e.xml';
        $urlNoWind = './s0000451_e.xml';
        $cachePath = sys_get_temp_dir();
        $cacheExpire = 120;

        $strings = [
            'TEXT_WIND'=>'Wind Conditions: ',
            'TEXT_FIP'=>'weather.gc.ca',
            'TEXT_TOGGLE'=>'Toggle this',
            'TEXT_GUST'=>'coup de vent',
            'FORMAT_LOCATION'=>'%s (%s)'
        ];
        
        $this->weatherWithCache = new \Ecweather\Weather($url, $cachePath, $cacheExpire);
        $this->weatherNoCache = new \Ecweather\Weather($url);
        $this->weatherNoWind = new \Ecweather\Weather($urlNoWind);
        $this->weatherStrings = new \Ecweather\Weather($url, null, null, $strings);

    }
    

    public function testSetIndent()
    {
        $object = $this->weatherNoCache->setIndent(2);
        $actual = \PHPUnit_Framework_Assert::readAttribute($this->weatherNoCache, 'indent');
        $this->assertEquals($actual, 2);
        $this->assertInstanceOf('Ecweather\Weather', $object);
    }

    public function testGetStationCode()
    {
        $expected = 'yvv';
        $actual = $this->weatherNoCache->getStationCode();
        $this->assertEquals($actual, $expected);
    }

    public function testGetLocationName()
    {
        $expected = 'South Bruce Peninsula, ON';
        $actual = $this->weatherNoCache->getLocationName();
        $this->assertEquals($actual, $expected);
    }

    public function testSetGetFipUrl()
    {
        $this->weatherNoCache->setFipUrl('http://example.com/fip.gif');
        $expected = 'http://example.com/fip.gif';
        $actual = $this->weatherNoCache->getFipUrl();
        $this->assertEquals($actual, $expected);
    }

    public function testSetGetIconBase()
    {
        $this->weatherNoCache->setIconBase('http://example.com/icons/');
        $expected = 'http://example.com/icons';
        $actual = $this->weatherNoCache->getIconBase();
        $this->assertEquals($actual, $expected);
    }

    public function testGetWarnings()
    {
        $expected = array(
            array(
                'url'=>'http://weather.gc.ca/warnings/report_e.html?on20',
                'type'=>'warning',
                'priority'=>'high',
                'description'=>'SNOW SQUALL WARNING IN EFFECT',
                'issued'=>'07:54 MST'
                )
        );

        $actual = $this->weatherNoCache->getWarnings();
        $this->assertEquals($actual, $expected);
    }

    public function testGetCurrentConditions()
    {
        $expected = [
            'icon'=>'08.gif',
            'condition'=>'Light Snowshower',
            'temperature'=>'0.1',
            'time'=>'10:24 MST',
            'wind'=>'W 23 km/h<br/>(gust 40)'
        ];

        $actual = $this->weatherNoCache->getCurrentConditions();
        $this->assertEquals($actual, $expected);
    }

    public function testGetForecastShort()
    {
        $first = [
            'icon'=>'16.gif',
            'period'=>'Today',
            'summary'=>'Temperature steady near plus 1.'
        ];
        $second = [
            'icon'=>'16.gif',
            'period'=>'Tonight',
            'summary'=>'Low minus 4.'
        ];
        $third = [
            'icon'=>'08.gif',
            'period'=>'Friday',
            'summary'=>'High minus 1.'
        ];
        $expected = [$first, $second, $third];

        $actual = $this->weatherNoCache->getForecastShort();
        $this->assertEquals($actual, $expected);
    }

    public function testGetForecastLong()
    {
        $first = [
            'icon'=>'16.gif',
            'period'=>'Thursday',
            'summary'=>'Flurries at times heavy and local snow squalls. Local amount 10 cm. Wind becoming northwest 30 km/h early this afternoon. Temperature steady near plus 1.'
        ];
        $second = [
            'icon'=>'16.gif',
            'period'=>'Thursday night',
            'summary'=>'Flurries ending after midnight then mainly cloudy with 30 percent chance of flurries. Local amount 2 cm. Wind northwest 30 km/h becoming light after midnight. Low minus 4.'
        ];
        $third = [
            'icon'=>'08.gif',
            'period'=>'Friday',
            'summary'=>'A mix of sun and cloud with 30 percent chance of flurries. Wind becoming northwest 20 km/h late in the evening. High minus 1.'
        ];
        $fourth = [
            'icon'=>'16.gif',
            'period'=>'Saturday',
            'summary'=>'Flurries. Low minus 5. High plus 2.'
        ];
        $fifth = [
            'icon'=>'16.gif',
            'period'=>'Sunday',
            'summary'=>'Flurries. Low minus 3. High plus 1.'
        ];
        $sixth = [
            'icon'=>'16.gif',
            'period'=>'Monday',
            'summary'=>'Flurries. Windy. Low minus 3. High minus 1.'
        ];
        $seventh = [
            'icon'=>'16.gif',
            'period'=>'Tuesday',
            'summary'=>'Cloudy with 70 percent chance of flurries or snow squalls. Low minus 6. High minus 1.'
        ];
        $eighth = [
            'icon'=>'16.gif',
            'period'=>'Wednesday',
            'summary'=>'Cloudy with 70 percent chance of flurries. Windy. Low minus 4. High zero.'
        ];
        $expected = [$first, $second, $third, $fourth, $fifth, $sixth, $seventh, $eighth];

        $actual = $this->weatherNoCache->getForecastLong();
        $this->assertEquals($actual, $expected);
    }

    public function testGetCurrentConditionsNoWind()
    {
        // This tests that the default 'Not Observed' should display for wind
        $expected = [
            'icon'=>false,
            'condition'=>'Not Observed',
            'temperature'=>'-0.5',
            'time'=>'11:00 MST',
            'wind'=>'Not Observed'
        ];

        $actual = $this->weatherNoWind->getCurrentConditions();

        $this->assertEquals($actual, $expected);
    }

    public function testDisplay()
    {
        // Note. Needed same encoding and line endings!
        $expected = file_get_contents('./output1.txt');

        $actual = $this->weatherNoCache->setIndent(2)->display();
        $this->assertSame($actual, $expected);
    }

    public function testDisplayStrings()
    {
        
        $expected = file_get_contents('./output2.txt');
        
        $actual = $this->weatherStrings->setIndent(2)->display();
        
        $this->assertSame($actual, $expected);
    }

    public function testDisplayForecastLink()
    {
        $expected = file_get_contents('./output3.txt');
        $this->weatherNoCache->setForecastLink('http://weather.gc.ca/pages/test.html');
        $actual = $this->weatherNoCache->setIndent(2)->display();
        $this->assertSame($actual, $expected);
    }

    public function testDisplayForecastBase()
    {
        $expected = file_get_contents('./output4.txt');
        $this->weatherNoCache->setForecastBase('http://weather.gc.ca/pages/test');
        $actual = $this->weatherNoCache->setIndent(2)->display();
        $this->assertSame($actual, $expected);
    }

    public function testDisplayCache()
    {
        // Note. Needed same encoding and line endings!
        $expected = file_get_contents('./output1.txt');

        $actual = $this->weatherWithCache->setIndent(2)->display();

        $this->assertSame($actual, $expected);


    } 

    public function testCache()
    {
        $expected = simplexml_load_file('./s0000727_e.xml');
        $actual = simplexml_load_file(sys_get_temp_dir() . '/s0000727_e.xml');

        $this->assertSame($actual->asXml(), $expected->asXml());
    }
}
