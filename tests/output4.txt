        <div class="ec-weather-widget">
            <div class="ec-warning warning"><a href="http://weather.gc.ca/warnings/report_e.html?on20" title="SNOW SQUALL WARNING IN EFFECT">!</a></div>
            <h1><a href="http://weather.gc.ca/pages/test/on-62_metric_e.html">South Bruce Peninsula, ON</a></h1>
            <div class="ec-current-weather">
                <div class="ec-weather-icon">
                    <img src="//weather.gc.ca/weathericons/08.gif" alt="Light Snowshower" title="Light Snowshower" />
                </div>
                <div class="ec-temp-wind">
                    <div class="ec-temp">0.1 &deg;C</div>
                    <div class="ec-wind">Wind: W 23 km/h<br/>(gust 40)</div>
                </div>
            </div>
            <div class="ec-forecast" data-toggle-text="Toggle Short-term Forecast">
                <div class="ec-forecast-container">
                    <div>Today</div>
                    <div class="ec-forecast-icon">
                        <img src="//weather.gc.ca/weathericons/16.gif" alt="Temperature steady near plus 1." title="Temperature steady near plus 1." />
                    </div>
                    <div class="ec-forecast-summary">Temperature steady near plus 1.</div>
                </div>
                <div class="ec-forecast-container">
                    <div>Tonight</div>
                    <div class="ec-forecast-icon">
                        <img src="//weather.gc.ca/weathericons/16.gif" alt="Low minus 4." title="Low minus 4." />
                    </div>
                    <div class="ec-forecast-summary">Low minus 4.</div>
                </div>
                <div class="ec-forecast-container">
                    <div>Friday</div>
                    <div class="ec-forecast-icon">
                        <img src="//weather.gc.ca/weathericons/08.gif" alt="High minus 1." title="High minus 1." />
                    </div>
                    <div class="ec-forecast-summary">High minus 1.</div>
                </div>
            </div>
            <div>
                <a href="http://weather.gc.ca/pages/test/on-62_metric_e.html" title="Environment Canada"><img src="//weather.gc.ca/images/ecfip_e.gif" alt="Environment Canada"/></a>
            </div>
        </div>