<?php
// This generates the wx_codes.data file for auto-generating the forecast urls. 
// Run this from the scripts directory from command line
// $ php wxo_xml.php
$xml_codes = [];

$sites_handle = fopen("site_list_en.csv", "r");
if ($sites_handle !== false) {
    while ($data = fgetcsv($sites_handle)) {
        if ($data !== false) {
            $xml_codes[$data[0]] = $data[1];
        }
    }

    fclose($sites_handle);
}

$wx_codes = [];
$wx_handle = fopen("wxo-url.txt", "r");
if ($wx_handle !== false) {
    while ($data = fgetcsv($wx_handle)) {
        if ($data !== false) {
            $last_slash_pos = strrpos($data[1], '/') + 1;
            $dash_pos = strpos($data[1], '_');
            $code = substr($data[1], $last_slash_pos, $dash_pos-$last_slash_pos);
            
            $xml_code = array_search($data[0], $xml_codes);
            if ($xml_code) {
                $wx_codes[$xml_code] = $code;
            }

            
        }
    }

    fclose($wx_handle);
}
ksort($wx_codes);
file_put_contents('../wx_codes.data', serialize($wx_codes));
